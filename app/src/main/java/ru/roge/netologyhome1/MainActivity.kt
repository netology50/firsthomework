package ru.roge.netologyhome1

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val TAG:String = "MainActivity"

        Log.d(TAG, "start of onCreate function")

        val firstName:String = "Ivan"
        val lastName:String = "Ivanov"
        val age:Int = 37
        val height:Double  = 172.2
        val textView: TextView = findViewById(R.id.output)
        textView.text = "name: $firstName surname: $lastName age: $age height: $height"

        Log.d(TAG, "end of onCreate function")


    }
}